//
//  TeacherMenuViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 23.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "TeacherMenuViewController.h"
#import "CoreData/Teacher.h"

@interface TeacherMenuViewController ()
@property (strong, nonatomic) Teacher *teacher;
@end

@implementation TeacherMenuViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.title = [NSString stringWithFormat:@"%@", self.teacher.firstname];
}

-(void)setManagedObject:(NSManagedObject *)object
{
    self.teacher = (Teacher *)object;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController conformsToProtocol:@protocol(HasManagedObjectField)]) {
        UIViewController<HasManagedObjectField> *controller = segue.destinationViewController;
        [controller setManagedObject:self.teacher];
    }
}

@end
