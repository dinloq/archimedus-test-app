//
//  DocumentationChaptersViewController.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 21.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractSelectableSearchViewController.h"

@interface ReferenceChaptersViewController : AbstractSelectableSearchViewController
@end
