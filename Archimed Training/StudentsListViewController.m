//
//  StudentsListViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 23.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "StudentsListViewController.h"
#import "TeacherStudentDetailedProfileViewController.h"
#import "CoreDataAccessor.h"
#import "CoreData/Student.h"
#import "CoreData/Grade.h"

@interface StudentsListViewController ()
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) Teacher *teacher;
@end

@implementation StudentsListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)setManagedObject:(NSManagedObject *)object
{
    self.teacher = (Teacher *)object;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];
    
    TeacherStudentDetailedProfileViewController *vc = segue.destinationViewController;
    [vc setTeacherObject:self.teacher];
}

#pragma mark - Overrided methods

-(NSArray *)loadData
{
    NSSortDescriptor *lastnameSort = [NSSortDescriptor sortDescriptorWithKey:@"secondname"
                                                                   ascending:YES];
    NSSortDescriptor *firstnameSort = [NSSortDescriptor sortDescriptorWithKey:@"firstname"
                                                                    ascending:YES];
    return [CoreDataAccessor getAllEntities:@"Student"
                                   withPredicate:nil
                                        withSort:@[lastnameSort, firstnameSort]];
}

-(Student *)convertToStudent:(NSManagedObject *)object
{
    return (Student *) object;
}

-(void)configureCell:(UITableViewCell *)cell withManagedObject:(NSManagedObject *)object
{
    Student *student = [self convertToStudent:object];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", student.secondname, student.firstname];
    cell.detailTextLabel.text = student.grade.classname;
}

-(NSString *)segueIdentifier
{
    return @"Profile";
}

-(NSString *)cellIdentifier
{
    return @"Cell";
}

-(NSString *)predicateString
{
    return @"SELF.firstname contains[c] $SEARCH OR SELF.secondname contains[c] $SEARCH";
}

@end
