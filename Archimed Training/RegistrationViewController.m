//
//  RegistrationViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 20.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "RegistrationViewController.h"
#import "CoreData/CoreDataAccessor.h"
#import "CoreData/Grade.h"
#import "StudentMenuViewController.h"

@interface RegistrationViewController  () <UITextFieldDelegate>
@property (strong, nonatomic) NSArray *grades;
@property (weak, nonatomic) IBOutlet UITextField *secondNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *loginTextField;
@end

@implementation RegistrationViewController 

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self fillPicker];
    [self.secondNameTextField setDelegate:self];
    [self.firstNameTextField setDelegate:self];
    [self.loginTextField setDelegate:self];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder *nextResponder = [self.view viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

-(void)fillPicker
{
    _grades = [CoreDataAccessor getAllEntities:@"Grade"];
    [_gradePicker setDataSource:self];
    [_gradePicker setDelegate:self];
}

#pragma mark - Picker Logic

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    Grade *grade = [_grades objectAtIndex:row];
    return grade.classname;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1; // assuming a single spinning wheel of strings (not split into left/right for example)
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [_grades count]; // the number of strings that your parser finds and adds to the array
}

#pragma mark - Registration Process

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"Registration"]) {
        if ([self studentExist]) {
            NSLog(@"fail");
            //print error of existing student
            return NO;
        }
    }
    return YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[StudentMenuViewController class]]) {
        if ([segue.identifier isEqualToString:@"Login"]) {
            StudentMenuViewController *controller = segue.destinationViewController;
            [controller setManagedObject:self.student];
        }
    }
    
    if (![segue.destinationViewController isKindOfClass:[RegistrationViewController class]]) {
        return;
    }
    if (![segue.identifier isEqualToString:@"Registration"]) {
        return;
    }
    Student *student = [self registerStudent];
    RegistrationViewController *contoller = (RegistrationViewController *) segue.destinationViewController;
    contoller.navigationItem.hidesBackButton = YES;
    contoller.student = student;
}

-(BOOL)studentExist
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"login = %@", self.loginTextField.text];
    NSArray *students = [CoreDataAccessor getAllEntities:@"Student" filterWith:predicate];
    
    return [students count];
}

-(Student *)registerStudent
{
    Student *student = [NSEntityDescription insertNewObjectForEntityForName:@"Student" inManagedObjectContext:[CoreDataAccessor managedContext]];
    
    student.firstname = self.firstNameTextField.text;
    student.secondname = self.secondNameTextField.text;
    student.login = self.loginTextField.text;
    NSInteger index = [self.gradePicker selectedRowInComponent:0];
    student.grade = [self.grades objectAtIndex:index];
    
    NSError *error;
    if (![[CoreDataAccessor managedContext] save:&error]) {
        NSLog(@"Something wrong, couldn't save: + %@", [error localizedDescription]);
        return nil;
    }
    
    return student;
}

@end
