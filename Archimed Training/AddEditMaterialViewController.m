//
//  AddEditMaterialViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 23.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "AddEditMaterialViewController.h"
#import "CoreData/CoreDataAccessor.h"
#import "CoreData/Material.h"

@interface AddEditMaterialViewController ()
@property (weak, nonatomic) IBOutlet UITextField *materialTextField;
@property (weak, nonatomic) IBOutlet UITextField *densityTextField;
@property (weak, nonatomic) IBOutlet UISwitch *isLiquidSwitch;

@end

@implementation AddEditMaterialViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - Abstract implementation

-(BOOL)isSaveRowIndexPath:(NSIndexPath *)indexPath
{
    return indexPath.row == 0 && indexPath.section == 1;
}

-(void)initFields
{
    [self.materialTextField setDelegate:self];
    [self.densityTextField setDelegate:self];
    
    Material *material = (Material *) self.entity;
    if (material) {
        self.materialTextField.text = material.name;
        self.densityTextField.text = material.density.stringValue;
        [self.isLiquidSwitch setOn:!material.solid.boolValue];
    }
}

-(NSString *)getEntityName
{
    return @"Material";
}

-(void)setEntityAttributes:(NSManagedObject *)entity
{
    Material *material = (Material *) entity;
    
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];

    material.name = self.materialTextField.text;
    material.density = [f numberFromString:self.densityTextField.text];
    material.solid = [NSNumber numberWithBool:!self.isLiquidSwitch.isOn];
}

@end
