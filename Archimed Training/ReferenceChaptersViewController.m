//
//  DocumentationChaptersViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 21.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "ReferenceChaptersViewController.h"
#import "CoreData/Guide.h"
#import "CoreData/CoreDataAccessor.h"
#import "Utils/ArchUtils.h"

@interface ReferenceChaptersViewController ()
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) Guide *guide;
@end

@implementation ReferenceChaptersViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(NSArray *)loadData
{
    return [CoreDataAccessor getAllEntities:@"Guide"];
}

-(Guide *)convertToGuide:(NSManagedObject *)object
{
    return (Guide *) object;
}

-(void)configureCell:(UITableViewCell *)cell withManagedObject:(NSManagedObject *)object
{
    cell.textLabel.text = [self convertToGuide:object].section;
}

-(NSString *)segueIdentifier
{
    return @"Show";
}

-(NSString *)cellIdentifier
{
    return @"Chapter";
}

-(NSString *)predicateString
{
    return @"SELF.text contains[c] $SEARCH OR SELF.section contains[c] $SEARCH";
}

@end
