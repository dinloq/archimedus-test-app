//
//  AbstractAddEditDataWithPickerViewController.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 27.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "AbstractAddEditDataViewController.h"
#import "Grade.h"

@interface AbstractAddEditDataWithPickerViewController : AbstractAddEditDataViewController <UIPickerViewDelegate, UIPickerViewDataSource>
-(Grade *)getSelectedGrade;

@end
