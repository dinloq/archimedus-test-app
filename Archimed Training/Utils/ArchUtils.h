//
//  ArchUtils.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 23.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Teacher.h"
#import "Student.h"

@interface ArchUtils : NSObject
@end

#pragma mark - Protocols
/**
 Protocol response if class has managed object like student, teacher or any core data
 */
@protocol HasManagedObjectField <NSObject>
-(void)setManagedObject:(NSManagedObject *)object;
@end


@protocol MessagerProtocol <NSObject>
-(void)initWithTeacher:(Teacher *)teacher student:(Student *)student isTeacher:(BOOL)isTeacher;
@end