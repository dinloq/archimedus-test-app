//
//  TrainingViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 20.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "TrainingViewController.h"
#import "AcuariumView.h"
#import "CoreData/Material.h"
#import "CoreData/CoreDataAccessor.h"
#import "Task.h"

@interface TrainingViewController ()
@property (weak, nonatomic) IBOutlet AcuariumView *bucketView;
@property (strong, nonatomic) NSArray *solids;
@property (strong, nonatomic) NSArray *liquids;
@property (weak, nonatomic) IBOutlet UIButton *upButton;
@property (weak, nonatomic) IBOutlet UIButton *downButton;
@property (weak, nonatomic) IBOutlet UIButton *taskButton;
@property (weak, nonatomic) IBOutlet UITextView *taskTextField;
@property (weak, nonatomic) IBOutlet UILabel *reviewLabel;
@property (strong, nonatomic) Task *task;
@property (nonatomic, strong) Student *student;
@end

@implementation TrainingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //custom initialization

    }
    return self;
}

- (void)viewDidLoad
{
    self.reviewLabel.text = @"";
    [super viewDidLoad];
    [self initMaterials];
    self.navigationItem.backBarButtonItem.title = @"В меню";
}

- (IBAction)giveAnswer:(UIButton *)sender
{
    [self hideControls:NO];
    BOOL isFloatAnswer = sender.tag == 1;
    BOOL result = isFloatAnswer == _task.willFloat;
    [self saveResult:result];
    
    [self.bucketView animateCubeFloat:_task.willFloat];
    [self giveReview:result];
    
    NSLog(@"%hhd ", result);
}

-(void)setManagedObject:(NSManagedObject *)object
{
    if ([object isKindOfClass:[Student class]]) self.student = (Student *) object;
}

#pragma mark - Task Changing Animation

- (IBAction)newQuestClick:(id)sender
{
    //Disapear old task
    [self hideReview];
    [UIView animateWithDuration:1
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         CGPoint outPoint = self.taskTextField.center;
                         outPoint.x = -self.view.bounds.size.width;
                         self.taskTextField.center = outPoint;
                     }
                     completion:^(BOOL finished) {
                         [self generateNewData];
                         [self newTaskAppear];
                     }];
}

-(void)newTaskAppear
{
    CGPoint begin = self.taskTextField.center;
    begin.x = self.view.bounds.size.width * 2;
    self.taskTextField.center = begin;
    
    [UIView animateWithDuration:1
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         CGPoint center = self.taskTextField.center;
                         center.x = self.view.bounds.size.width / 2;
                         self.taskTextField.center = center;
                     }
                     completion:^(BOOL finished) {
                         [self hideControls:YES];
                     }];
}

-(void)hideReview
{
    CGPoint outPoint = self.reviewLabel.center;
    outPoint.x = -self.view.bounds.size.width;
    
    [UIView animateWithDuration:1 animations:^{
        self.reviewLabel.center = outPoint;
    }];
}

-(void)giveReview:(BOOL)isRight
{
    [self setReviewText:isRight];
    
    CGPoint beginPoint = self.reviewLabel.center;
    beginPoint.x = self.view.bounds.size.width * 2;
    self.reviewLabel.center = beginPoint;
    
    [UIView animateWithDuration:1
                          delay:1
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         CGPoint center = beginPoint;
                         center.x = self.view.bounds.size.width / 2;
                         self.reviewLabel.center = center;
                         
                     } completion:nil];
}

-(void)setReviewText:(BOOL)isRight
{
    NSString *text = (isRight) ? @"Правильно!" : @"Неверно!";
    UIColor *color = (isRight) ? [UIColor greenColor] : [UIColor redColor];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:text];
    [string addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0, text.length)];
    
    [self.reviewLabel setAttributedText:string];
}

#pragma mark - Business Logic

-(void)saveResult:(BOOL)isRight
{
    NSInteger num = ((isRight) ? self.student.right : self.student.wrong).integerValue + 1;
    if (isRight) {
        self.student.right = [NSNumber numberWithInteger:num];
    } else {
        self.student.wrong = [NSNumber numberWithInteger:num];
    }
    NSError *err = nil;
    if (![[CoreDataAccessor managedContext] save:&err]) {
        NSLog(@"Update error:%@", [err localizedDescription]);
    }
}

-(void)generateNewData
{
    Material *body = [self.solids objectAtIndex:arc4random() % [self.solids count]];
    Material *liquid = [self.liquids objectAtIndex:arc4random() % [self.liquids count]];

    self.task = [[Task alloc] initWithBody:body liquid:liquid];
    [self.taskTextField setAttributedText:_task.description];
}

-(void)hideControls:(BOOL)isDesigionControls
{
    self.taskButton.enabled = !isDesigionControls;
    self.upButton.enabled = isDesigionControls;
    self.downButton.enabled = isDesigionControls;
}

-(void)initMaterials
{
    NSPredicate *predicate = nil;
    NSString *predicateString = @"solid = %@";
    
    predicate = [NSPredicate predicateWithFormat:predicateString, [NSNumber numberWithBool:YES]];
    self.solids = [CoreDataAccessor getAllEntities:@"Material" filterWith:predicate];
    
    predicate = [NSPredicate predicateWithFormat:predicateString, [NSNumber numberWithBool:NO]];
    self.liquids = [CoreDataAccessor getAllEntities:@"Material" filterWith:predicate];
}

@end
