//
//  TeachersListViewController.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 28.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "AbstractSelectableSearchViewController.h"

@interface TeachersListViewController : AbstractSelectableSearchViewController <HasManagedObjectField>

@end
