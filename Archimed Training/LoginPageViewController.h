//
//  LoginPageViewController.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 02.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utils/ArchUtils.h"

@interface LoginPageViewController : UITableViewController
@property (nonatomic) BOOL isTeacher;
@end

