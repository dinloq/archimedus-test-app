//
//  main.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 24.02.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HMIAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HMIAppDelegate class]));
    }
}
