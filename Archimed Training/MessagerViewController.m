//
//  MessagerViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 28.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "MessagerViewController.h"
#import "PTSMessagingCell.h"


@interface MessagerViewController ()
@property (strong, nonatomic) Teacher *teacher;
@property (strong, nonatomic) Student *student;
@property (nonatomic) BOOL isTeacher;
@property (weak, nonatomic) IBOutlet UITableView *innerTableView;
@property (strong, nonatomic) NSArray *messages;
@property (weak, nonatomic) IBOutlet UITextField *messageTF;

@property(nonatomic) Boolean keyboardIsShowing;
@property(nonatomic) CGRect keyboardBounds;
- (void)resizeViewControllerToFitScreen;
@end

@implementation MessagerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
       
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.innerTableView setDelegate:self];
    [self.innerTableView setDataSource:self];
    [self.messageTF setDelegate:self];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self scrollDownTableViewAnimated:NO withKeyboardShown:YES];
}

-(void)initWithTeacher:(Teacher *)teacher student:(Student *)student isTeacher:(BOOL)isTeacher
{
    self.teacher = teacher;
    self.student = student;
    self.isTeacher = isTeacher;
}

-(void)sendMessage:(NSString *)text
{
    if ([text length] < 1) {
        return;
    }
    
    Message *message = [NSEntityDescription insertNewObjectForEntityForName:@"Message" inManagedObjectContext:[CoreDataAccessor managedContext]];
    
    message.teacher = self.teacher;
    message.student = self.student;
    message.isFromTeacher = [NSNumber numberWithBool:self.isTeacher];
    message.text = text;
    message.date = [NSDate date];
    
    NSError *error;
    if (![[CoreDataAccessor managedContext] save:&error]) {
        NSLog(@"Something wrong, couldn't save: + %@", [error localizedDescription]);
    }
    
    self.messageTF.text = @"";
    
    self.messages = nil;
    [self.innerTableView reloadData];
    [self scrollDownTableViewAnimated:YES withKeyboardShown:YES];
}

-(void)scrollDownTableViewAnimated:(BOOL)animated withKeyboardShown:(BOOL)shownKeyboard
{
    CGFloat yOffset = self.innerTableView.contentSize.height - self.innerTableView.frame.size.height;
    
    CGPoint offset = (shownKeyboard) ? CGPointMake(0, yOffset - self.keyboardBounds.size.height) : CGPointMake(0, yOffset);
    [self.innerTableView setContentOffset:offset animated:animated];
}

#pragma mark - TextField returner

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self sendMessage:textField.text];
    [textField resignFirstResponder];
    return NO; // We do not want UITextField to insert line-breaks.
}

#pragma mark - Show Hide Keyboard logic

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)resizeViewControllerToFitScreen
{
    // Needs adjustment for portrait orientation!
	CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
	CGRect frame = self.view.frame;
	frame.size.height = applicationFrame.size.height - self.navigationController.navigationBar.frame.size.height;
    
    
	if (_keyboardIsShowing) {
		frame.size.height -= _keyboardBounds.size.height;
        [self scrollDownTableViewAnimated:YES withKeyboardShown:NO];
    }
    
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:0.3f];
	self.view.frame = frame;
	[UIView commitAnimations];
}

- (void)keyboardWillShow:(NSNotification *)notification {
	NSDictionary *userInfo = [notification userInfo];
	NSValue *keyboardBoundsValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
	[keyboardBoundsValue getValue:&_keyboardBounds];
	_keyboardIsShowing = YES;
	[self resizeViewControllerToFitScreen];
}

- (void)keyboardWillHide:(NSNotification *)note {
	_keyboardIsShowing = NO;
	_keyboardBounds = CGRectMake(0, 0, 0, 0);
	[self resizeViewControllerToFitScreen];
}


#pragma mark - Table view Logic

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.messages count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PTSMessagingCell *cell = (PTSMessagingCell *) [self.innerTableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    
    cell = [[PTSMessagingCell alloc] initMessagingCellWithReuseIdentifier:@"Cell"];
    
    [self configureCell:cell withManagedObject:[self.messages objectAtIndex:indexPath.row]];
    return cell;
}

#pragma mark - Abstract List View Controller methods

-(NSArray *)messages
{
    if (!_messages) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.teacher = %@ AND SELF.student = %@", self.teacher.objectID, self.student.objectID];
        
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES];
        
        _messages =  [CoreDataAccessor getAllEntities:@"Message"
                                  withPredicate:predicate
                                       withSort:@[sort]];
    }
    return _messages;
}

-(void)configureCell:(PTSMessagingCell *)cell withManagedObject:(NSManagedObject *)object
{
    Message *message = (Message *)object;

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM - HH:mm:ss"];
    NSString *date  = [dateFormatter stringFromDate:message.date];
    
    
    cell.messageLabel.text = message.text;
    cell.timeLabel.text = date;
    cell.sent = message.isFromTeacher.boolValue != self.isTeacher;
}

@end
