//
//  Message.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 20.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Student, Teacher;

@interface Message : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSNumber * isFromTeacher;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) Student *student;
@property (nonatomic, retain) Teacher *teacher;

@end
