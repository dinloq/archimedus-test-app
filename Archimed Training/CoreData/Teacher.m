//
//  Teacher.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 20.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "Teacher.h"
#import "Grade.h"
#import "Message.h"


@implementation Teacher

@dynamic firstname;
@dynamic login;
@dynamic secondname;
@dynamic grade;
@dynamic messages;

@end
