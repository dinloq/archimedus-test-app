//
//  Material.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 20.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Material : NSManagedObject

@property (nonatomic, retain) NSNumber * density;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * solid;

@end
