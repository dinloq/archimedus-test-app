//
//  Student.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 20.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "Student.h"
#import "Grade.h"
#import "Message.h"


@implementation Student

@dynamic firstname;
@dynamic login;
@dynamic right;
@dynamic secondname;
@dynamic wrong;
@dynamic grade;
@dynamic messages;

@end
