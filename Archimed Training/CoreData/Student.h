//
//  Student.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 20.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Grade, Message;

@interface Student : NSManagedObject

@property (nonatomic, retain) NSString * firstname;
@property (nonatomic, retain) NSString * login;
@property (nonatomic, retain) NSNumber * right;
@property (nonatomic, retain) NSString * secondname;
@property (nonatomic, retain) NSNumber * wrong;
@property (nonatomic, retain) Grade *grade;
@property (nonatomic, retain) NSSet *messages;
@end

@interface Student (CoreDataGeneratedAccessors)

- (void)addMessagesObject:(Message *)value;
- (void)removeMessagesObject:(Message *)value;
- (void)addMessages:(NSSet *)values;
- (void)removeMessages:(NSSet *)values;

@end
