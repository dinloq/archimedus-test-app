//
//  Guide.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 20.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Guide : NSManagedObject

@property (nonatomic, retain) NSString * section;
@property (nonatomic, retain) NSString * text;

@end
