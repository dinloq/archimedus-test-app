//
//  Message.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 20.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "Message.h"
#import "Student.h"
#import "Teacher.h"


@implementation Message

@dynamic date;
@dynamic isFromTeacher;
@dynamic text;
@dynamic student;
@dynamic teacher;

@end
