//
//  CoreDataAccessor.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 20.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoreDataAccessor : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+(NSManagedObjectContext *)managedContext;
+(NSArray *)getAllEntities:(NSString *)name;
+(NSArray *)getAllEntities:(NSString *)name filterWith:(NSPredicate *)predicate;
+(NSArray *)getAllEntities:(NSString *)name withPredicate:(NSPredicate *)predicate withSort:(NSArray *)sortDescriptors;

+(void)removeObjects:(NSArray *)array;

-(void)saveContext;

@end
