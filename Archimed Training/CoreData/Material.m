//
//  Material.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 20.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "Material.h"


@implementation Material

@dynamic density;
@dynamic name;
@dynamic solid;

@end
