//
//  Grade.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 20.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "Grade.h"
#import "Student.h"
#import "Teacher.h"


@implementation Grade

@dynamic classname;
@dynamic students;
@dynamic teacher;

@end
