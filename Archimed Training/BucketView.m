//
//  BucketView.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 26.02.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "BucketView.h"

@interface BucketView()

@end

@implementation BucketView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

-(void)setup
{
    self.backgroundColor = nil;
    self.opaque = NO;
}

-(void)awakeFromNib
{
    [self setup];
}

- (void)drawRect:(CGRect)rect
{
    [self drawBucket];
    [UIView animateWithDuration:1
                          delay:0
                        options:UIViewAnimationOptionRepeat
                     animations:^
     {
         _textureOffset++;
     }
                     completion:nil];
}

-(void) drawBucket
{
    //// General Declarations
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //// Color Declarations
    UIColor* gradientColor = [UIColor colorWithRed: 0.574 green: 0.684 blue: 1 alpha: 0.503];
    UIColor* gradientColor2 = [UIColor colorWithRed: 0.173 green: 0.239 blue: 0.302 alpha: 1];
    UIColor* gradientColor3 = [UIColor colorWithRed: 0.28 green: 0.28 blue: 0.28 alpha: 0.292];
    UIColor* gradientColor4 = [UIColor colorWithRed: 1 green: 1 blue: 1 alpha: 0.142];
    
    //// Gradient Declarations
    NSArray* waterGradientColors = [NSArray arrayWithObjects:
                                    (id)gradientColor2.CGColor,
                                    (id)[UIColor colorWithRed: 0.373 green: 0.462 blue: 0.651 alpha: 0.752].CGColor,
                                    (id)gradientColor.CGColor, nil];
    CGFloat waterGradientLocations[] = {0, 0.41, 1};
    CGGradientRef waterGradient = CGGradientCreateWithColors(colorSpace, (CFArrayRef)waterGradientColors, waterGradientLocations);
    NSArray* glassGradientColors = [NSArray arrayWithObjects:
                                    (id)gradientColor3.CGColor,
                                    (id)gradientColor4.CGColor, nil];
    CGFloat glassGradientLocations[] = {0, 1};
    CGGradientRef glassGradient = CGGradientCreateWithColors(colorSpace, (CFArrayRef)glassGradientColors, glassGradientLocations);
    
    //// Shadow Declarations
    UIColor* shadow = [UIColor blackColor];
    CGSize shadowOffset = CGSizeMake(2.1, 2.1);
    CGFloat shadowBlurRadius = 6;
    
    //// Frames
    CGRect frame = self.bounds;
    
    //// Subframes
    CGRect cupFront = CGRectMake(CGRectGetMinX(frame) + floor(CGRectGetWidth(frame) * 0.07018 + 0.5), CGRectGetMinY(frame) + floor(CGRectGetHeight(frame) * 0.04255 + 0.5), floor(CGRectGetWidth(frame) * 0.93860 + 0.5) - floor(CGRectGetWidth(frame) * 0.07018 + 0.5), floor(CGRectGetHeight(frame) * 0.94681 + 0.5) - floor(CGRectGetHeight(frame) * 0.04255 + 0.5));
    
    
    //// Cup front
    {
        //// Water Bezier Drawing
        UIBezierPath* waterBezierPath = [UIBezierPath bezierPath];
        [waterBezierPath moveToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.81901 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.94487 * CGRectGetHeight(cupFront))];
        [waterBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.82244 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.94487 * CGRectGetHeight(cupFront))];
        [waterBezierPath addCurveToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.81781 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.95086 * CGRectGetHeight(cupFront)) controlPoint1: CGPointMake(CGRectGetMinX(cupFront) + 0.82158 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.94690 * CGRectGetHeight(cupFront)) controlPoint2: CGPointMake(CGRectGetMinX(cupFront) + 0.82002 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.94890 * CGRectGetHeight(cupFront))];
        [waterBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.81579 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.96093 * CGRectGetHeight(cupFront))];
        [waterBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.79997 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.96093 * CGRectGetHeight(cupFront))];
        [waterBezierPath addCurveToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.47745 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.99399 * CGRectGetHeight(cupFront)) controlPoint1: CGPointMake(CGRectGetMinX(cupFront) + 0.74908 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.98168 * CGRectGetHeight(cupFront)) controlPoint2: CGPointMake(CGRectGetMinX(cupFront) + 0.62093 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.99563 * CGRectGetHeight(cupFront))];
        [waterBezierPath addCurveToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.20007 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.96094 * CGRectGetHeight(cupFront)) controlPoint1: CGPointMake(CGRectGetMinX(cupFront) + 0.35119 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.99254 * CGRectGetHeight(cupFront)) controlPoint2: CGPointMake(CGRectGetMinX(cupFront) + 0.24519 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.97931 * CGRectGetHeight(cupFront))];
        [waterBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.18205 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.96093 * CGRectGetHeight(cupFront))];
        [waterBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.17946 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.94800 * CGRectGetHeight(cupFront))];
        [waterBezierPath addCurveToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.17816 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.94609 * CGRectGetHeight(cupFront)) controlPoint1: CGPointMake(CGRectGetMinX(cupFront) + 0.17896 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.94737 * CGRectGetHeight(cupFront)) controlPoint2: CGPointMake(CGRectGetMinX(cupFront) + 0.17853 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.94673 * CGRectGetHeight(cupFront))];
        [waterBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.04040 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.28235 * CGRectGetHeight(cupFront))];
        [waterBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.35354 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.28235 * CGRectGetHeight(cupFront))];
        [waterBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.49495 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.35294 * CGRectGetHeight(cupFront))];
        [waterBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.63636 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.28235 * CGRectGetHeight(cupFront))];
        [waterBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.95160 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.28305 * CGRectGetHeight(cupFront))];
        [waterBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.81901 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.94487 * CGRectGetHeight(cupFront))];
        [waterBezierPath closePath];
        CGContextSaveGState(context);
        [waterBezierPath addClip];
        CGRect waterBezierBounds = CGPathGetPathBoundingBox(waterBezierPath.CGPath);
        CGContextDrawLinearGradient(context, waterGradient,
                                    CGPointMake(CGRectGetMinX(waterBezierBounds), CGRectGetMidY(waterBezierBounds)),
                                    CGPointMake(CGRectGetMaxX(waterBezierBounds), CGRectGetMidY(waterBezierBounds)),
                                    0);
        CGContextRestoreGState(context);
        
        
        //// Oval Drawing
        UIBezierPath* ovalPath = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(CGRectGetMinX(cupFront) + floor(CGRectGetWidth(cupFront) * 0.00505) + 0.5, CGRectGetMinY(cupFront) + floor(CGRectGetHeight(cupFront) * 0.00588) + 0.5, floor(CGRectGetWidth(cupFront) * 0.99495) - floor(CGRectGetWidth(cupFront) * 0.00505), floor(CGRectGetHeight(cupFront) * 0.17059) - floor(CGRectGetHeight(cupFront) * 0.00588))];
        [[UIColor blackColor] setStroke];
        ovalPath.lineWidth = 1;
        [ovalPath stroke];
        
        
        //// Bezier Drawing
        UIBezierPath* bezierPath = [UIBezierPath bezierPath];
        [bezierPath moveToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.00505 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.10000 * CGRectGetHeight(cupFront))];
        [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.17677 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.94706 * CGRectGetHeight(cupFront))];
        [[UIColor clearColor] setFill];
        [bezierPath fill];
        CGContextSaveGState(context);
        CGContextSetShadowWithColor(context, shadowOffset, shadowBlurRadius, shadow.CGColor);
        [[UIColor blackColor] setStroke];
        bezierPath.lineWidth = 1;
        [bezierPath stroke];
        CGContextRestoreGState(context);
        
        
        //// Bottom CUp Drawing
        CGRect bottomCUpRect = CGRectMake(CGRectGetMinX(cupFront) + floor(CGRectGetWidth(cupFront) * 0.17677) + 0.5, CGRectGetMinY(cupFront) + floor(CGRectGetHeight(cupFront) * 0.88824) + 0.5, floor(CGRectGetWidth(cupFront) * 0.82323) - floor(CGRectGetWidth(cupFront) * 0.17677), floor(CGRectGetHeight(cupFront) * 0.99412) - floor(CGRectGetHeight(cupFront) * 0.88824));
        UIBezierPath* bottomCUpPath = [UIBezierPath bezierPath];
        [bottomCUpPath addArcWithCenter: CGPointMake(0, 0) radius: CGRectGetWidth(bottomCUpRect) / 2 startAngle: 0 * M_PI/180 endAngle: 180 * M_PI/180 clockwise: YES];
        
        CGAffineTransform bottomCUpTransform = CGAffineTransformMakeTranslation(CGRectGetMidX(bottomCUpRect), CGRectGetMidY(bottomCUpRect));
        bottomCUpTransform = CGAffineTransformScale(bottomCUpTransform, 1, CGRectGetHeight(bottomCUpRect) / CGRectGetWidth(bottomCUpRect));
        [bottomCUpPath applyTransform: bottomCUpTransform];
        
        [[UIColor blackColor] setStroke];
        bottomCUpPath.lineWidth = 1;
        [bottomCUpPath stroke];
        
        
        //// Bezier 3 Drawing
        UIBezierPath* bezier3Path = [UIBezierPath bezierPath];
        [bezier3Path moveToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.99495 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.08824 * CGRectGetHeight(cupFront))];
        [bezier3Path addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.82323 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.94706 * CGRectGetHeight(cupFront))];
        [[UIColor clearColor] setFill];
        [bezier3Path fill];
        CGContextSaveGState(context);
        CGContextSetShadowWithColor(context, shadowOffset, shadowBlurRadius, shadow.CGColor);
        [[UIColor blackColor] setStroke];
        bezier3Path.lineWidth = 1;
        [bezier3Path stroke];
        CGContextRestoreGState(context);
        
        
        //// Glass Bezier Drawing
        UIBezierPath* glassBezierPath = [UIBezierPath bezierPath];
        [glassBezierPath moveToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.00505 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.08824 * CGRectGetHeight(cupFront))];
        [glassBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.17677 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.93529 * CGRectGetHeight(cupFront))];
        [glassBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.18687 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.95882 * CGRectGetHeight(cupFront))];
        [glassBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.27778 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.98235 * CGRectGetHeight(cupFront))];
        [glassBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.44949 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.99412 * CGRectGetHeight(cupFront))];
        [glassBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.63131 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.99412 * CGRectGetHeight(cupFront))];
        [glassBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.75253 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.97059 * CGRectGetHeight(cupFront))];
        [glassBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.82323 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.94706 * CGRectGetHeight(cupFront))];
        [glassBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.99495 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.08824 * CGRectGetHeight(cupFront))];
        [glassBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.97475 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.11176 * CGRectGetHeight(cupFront))];
        [glassBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.90404 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.13529 * CGRectGetHeight(cupFront))];
        [glassBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.75253 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.15882 * CGRectGetHeight(cupFront))];
        [glassBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.60101 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.17059 * CGRectGetHeight(cupFront))];
        [glassBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.42929 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.17059 * CGRectGetHeight(cupFront))];
        [glassBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.22727 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.15882 * CGRectGetHeight(cupFront))];
        [glassBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.08586 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.13529 * CGRectGetHeight(cupFront))];
        [glassBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.03535 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.12353 * CGRectGetHeight(cupFront))];
        [glassBezierPath addLineToPoint: CGPointMake(CGRectGetMinX(cupFront) + 0.00505 * CGRectGetWidth(cupFront), CGRectGetMinY(cupFront) + 0.08824 * CGRectGetHeight(cupFront))];
        [glassBezierPath closePath];
        CGContextSaveGState(context);
        [glassBezierPath addClip];
        CGRect glassBezierBounds = CGPathGetPathBoundingBox(glassBezierPath.CGPath);
        CGContextDrawLinearGradient(context, glassGradient,
                                    CGPointMake(CGRectGetMinX(glassBezierBounds), CGRectGetMidY(glassBezierBounds)),
                                    CGPointMake(CGRectGetMaxX(glassBezierBounds), CGRectGetMidY(glassBezierBounds)),
                                    0);
        CGContextRestoreGState(context);
        [[UIColor blackColor] setStroke];
        glassBezierPath.lineWidth = 1;
        [glassBezierPath stroke];
    }
    
    
    //// Cleanup
    CGGradientRelease(waterGradient);
    CGGradientRelease(glassGradient);
    CGColorSpaceRelease(colorSpace);
}

-(void)incTextureOffset
{
    _textureOffset ++;
    if (_textureOffset > 100) {
        _textureOffset = 0;
    }
}

@end
