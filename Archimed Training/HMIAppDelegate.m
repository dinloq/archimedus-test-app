//
//  HMIAppDelegate.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 24.02.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "HMIAppDelegate.h"
#import "CoreData/CoreDataAccessor.h"
#import "CoreData/Message.h"

@interface HMIAppDelegate()
@property(strong, nonatomic) CoreDataAccessor *coreDataAccessor;
@end

@implementation HMIAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [self setUIComponentsStyle];
    self.coreDataAccessor = [[CoreDataAccessor alloc] init];
    
    return YES;
}

-(void)setUIComponentsStyle
{
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setBackgroundColor:MAIN_COLOR];
    [[UINavigationBar appearance] setTintColor:MAIN_COLOR];
    [UITableViewCell appearance].selectionStyle = UITableViewCellSelectionStyleGray;
}

@end
