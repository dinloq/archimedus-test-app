//
//  StudentProfileViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 21.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "StudentProfileViewController.h"
#import "CoreData/Student.h"
#import "CoreData/Grade.h"

@interface StudentProfileViewController ()

@property (weak, nonatomic) IBOutlet UITableViewCell *nameCell;

@property (weak, nonatomic) IBOutlet UITableViewCell *gradeCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *loginCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *rightCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *wrongCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *ratingCell;

@end

@implementation StudentProfileViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSInteger right = self.student.right.integerValue;
    NSInteger wrong = self.student.wrong.integerValue;
    
    self.nameCell.textLabel.text = [NSString stringWithFormat:@"%@ %@", self.student.firstname, self.student.secondname];
    
    self.gradeCell.detailTextLabel.text = self.student.grade.classname;
    self.loginCell.detailTextLabel.text = self.student.login;
    self.rightCell.detailTextLabel.text = [NSString stringWithFormat:@"%d", right];
    self.wrongCell.detailTextLabel.text = [NSString stringWithFormat:@"%d", wrong];
    self.ratingCell.detailTextLabel.text = [NSString stringWithFormat:@"%.2f%@", [self countRatingWithRigh:right wrong:wrong], @"%"];
}

-(double)countRatingWithRigh:(NSInteger)right wrong:(NSInteger)wrong
{
    if (wrong == 0 ) return 100;
    return 100.0 * right / (right + wrong);
}

-(void)setManagedObject:(NSManagedObject *)object
{
    if ([object isKindOfClass:[Student class]]) self.student = (Student *) object;
}

@end
