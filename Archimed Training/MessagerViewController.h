//
//  MessagerViewController.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 28.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "AbstractSelectableSearchViewController.h"
#import "CoreDataAccessor.h"
#import "Message.h"
#import "Teacher.h"
#import "Grade.h"
#import "Student.h"
#import "Utils/ArchUtils.h"

@interface MessagerViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, MessagerProtocol>

@end

