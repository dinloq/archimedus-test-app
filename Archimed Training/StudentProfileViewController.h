//
//  StudentProfileViewController.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 21.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utils/ArchUtils.h"

@interface StudentProfileViewController : UITableViewController <HasManagedObjectField>
@property (strong, nonatomic) Student *student;
@end
