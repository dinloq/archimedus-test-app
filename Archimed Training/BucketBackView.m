//
//  BucketBackView.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 01.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "BucketBackView.h"

@implementation BucketBackView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

-(void)setup
{
    self.backgroundColor = nil;
    self.opaque = NO;
}

- (void)drawRect:(CGRect)rect
{
    [self drawBucketBack];
}

-(void)drawBucketBack
{
    //// General Declarations
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //// Color Declarations
    UIColor* gradientColor = [UIColor colorWithRed: 0.574 green: 0.684 blue: 1 alpha: 0.503];
    UIColor* gradientColor2 = [UIColor colorWithRed: 0.173 green: 0.239 blue: 0.302 alpha: 1];
    UIColor* shadow2Color = [UIColor colorWithRed: 0 green: 0 blue: 0 alpha: 1];
    UIColor* color3 = [UIColor colorWithRed: 0 green: 0 blue: 0 alpha: 0.235];
    UIColor* gradientColor3 = [UIColor colorWithRed: 0.28 green: 0.28 blue: 0.28 alpha: 0.292];
    UIColor* gradientColor4 = [UIColor colorWithRed: 1 green: 1 blue: 1 alpha: 0.142];
    
    //// Gradient Declarations
    NSArray* waterGradientColors = [NSArray arrayWithObjects:
                                    (id)gradientColor2.CGColor,
                                    (id)[UIColor colorWithRed: 0.373 green: 0.462 blue: 0.651 alpha: 0.752].CGColor,
                                    (id)gradientColor.CGColor, nil];
    CGFloat waterGradientLocations[] = {0, 0.41, 1};
    CGGradientRef waterGradient = CGGradientCreateWithColors(colorSpace, (CFArrayRef)waterGradientColors, waterGradientLocations);
    NSArray* glassGradientColors = [NSArray arrayWithObjects:
                                    (id)gradientColor3.CGColor,
                                    (id)gradientColor4.CGColor, nil];
    CGFloat glassGradientLocations[] = {0, 1};
    CGGradientRef glassGradient = CGGradientCreateWithColors(colorSpace, (CFArrayRef)glassGradientColors, glassGradientLocations);
    
    //// Shadow Declarations
    UIColor* shadow2 = shadow2Color;
    CGSize shadow2Offset = CGSizeMake(-1.1, 1.1);
    CGFloat shadow2BlurRadius = 4.5;
    
    //// Frames
    CGRect frame = self.bounds;
    
    //// Subframes
    CGRect cupBac = CGRectMake(CGRectGetMinX(frame) + floor(CGRectGetWidth(frame) * 0.06140 + 0.5), CGRectGetMinY(frame) + floor(CGRectGetHeight(frame) * 0.03191 + 0.5), floor(CGRectGetWidth(frame) * 0.94737 + 0.5) - floor(CGRectGetWidth(frame) * 0.06140 + 0.5), floor(CGRectGetHeight(frame) * 0.94681 + 0.5) - floor(CGRectGetHeight(frame) * 0.03191 + 0.5));
    
    
    //// cup bac
    {
        //// Glass Back Drawing
        CGRect glassBackRect = CGRectMake(CGRectGetMinX(cupBac) + floor(CGRectGetWidth(cupBac) * 0.00495) + 0.5, CGRectGetMinY(cupBac) + floor(CGRectGetHeight(cupBac) * 0.00581) + 0.5, floor(CGRectGetWidth(cupBac) * 0.99505) - floor(CGRectGetWidth(cupBac) * 0.00495), floor(CGRectGetHeight(cupBac) * 0.19186) - floor(CGRectGetHeight(cupBac) * 0.00581));
        UIBezierPath* glassBackPath = [UIBezierPath bezierPathWithOvalInRect: glassBackRect];
        CGContextSaveGState(context);
        [glassBackPath addClip];
        CGContextDrawLinearGradient(context, glassGradient,
                                    CGPointMake(CGRectGetMinX(glassBackRect), CGRectGetMidY(glassBackRect)),
                                    CGPointMake(CGRectGetMaxX(glassBackRect), CGRectGetMidY(glassBackRect)),
                                    0);
        CGContextRestoreGState(context);
        
        
        //// Back Bottom Drawing
        CGRect backBottomRect = CGRectMake(CGRectGetMinX(cupBac) + floor(CGRectGetWidth(cupBac) * 0.17822 + 0.5), CGRectGetMinY(cupBac) + floor(CGRectGetHeight(cupBac) * 0.88372 + 0.5), floor(CGRectGetWidth(cupBac) * 0.82178 + 0.5) - floor(CGRectGetWidth(cupBac) * 0.17822 + 0.5), floor(CGRectGetHeight(cupBac) * 1.00000 + 0.5) - floor(CGRectGetHeight(cupBac) * 0.88372 + 0.5));
        UIBezierPath* backBottomPath = [UIBezierPath bezierPath];
        [backBottomPath addArcWithCenter: CGPointMake(0, 0) radius: CGRectGetWidth(backBottomRect) / 2 startAngle: 180 * M_PI/180 endAngle: 0 * M_PI/180 clockwise: YES];
        
        CGAffineTransform backBottomTransform = CGAffineTransformMakeTranslation(CGRectGetMidX(backBottomRect), CGRectGetMidY(backBottomRect));
        backBottomTransform = CGAffineTransformScale(backBottomTransform, 1, CGRectGetHeight(backBottomRect) / CGRectGetWidth(backBottomRect));
        [backBottomPath applyTransform: backBottomTransform];
        
        CGContextSaveGState(context);
        CGContextSetShadowWithColor(context, shadow2Offset, shadow2BlurRadius, shadow2.CGColor);
        CGContextRestoreGState(context);
        
        [color3 setStroke];
        backBottomPath.lineWidth = 1;
        [backBottomPath stroke];
        
        
        //// Cup Back 2 Drawing
        UIBezierPath* cupBack2Path = [UIBezierPath bezierPath];
        [cupBack2Path moveToPoint: CGPointMake(CGRectGetMinX(cupBac) + 0.63366 * CGRectGetWidth(cupBac), CGRectGetMinY(cupBac) + 0.29023 * CGRectGetHeight(cupBac))];
        [cupBack2Path addLineToPoint: CGPointMake(CGRectGetMinX(cupBac) + 0.49505 * CGRectGetWidth(cupBac), CGRectGetMinY(cupBac) + 0.36047 * CGRectGetHeight(cupBac))];
        [cupBack2Path addLineToPoint: CGPointMake(CGRectGetMinX(cupBac) + 0.35644 * CGRectGetWidth(cupBac), CGRectGetMinY(cupBac) + 0.29023 * CGRectGetHeight(cupBac))];
        [cupBack2Path addLineToPoint: CGPointMake(CGRectGetMinX(cupBac) + 0.31332 * CGRectGetWidth(cupBac), CGRectGetMinY(cupBac) + 0.29023 * CGRectGetHeight(cupBac))];
        [cupBack2Path addLineToPoint: CGPointMake(CGRectGetMinX(cupBac) + 0.05446 * CGRectGetWidth(cupBac), CGRectGetMinY(cupBac) + 0.29023 * CGRectGetHeight(cupBac))];
        [cupBack2Path addCurveToPoint: CGPointMake(CGRectGetMinX(cupBac) + 0.50000 * CGRectGetWidth(cupBac), CGRectGetMinY(cupBac) + 0.22674 * CGRectGetHeight(cupBac)) controlPoint1: CGPointMake(CGRectGetMinX(cupBac) + 0.05446 * CGRectGetWidth(cupBac), CGRectGetMinY(cupBac) + 0.25517 * CGRectGetHeight(cupBac)) controlPoint2: CGPointMake(CGRectGetMinX(cupBac) + 0.25393 * CGRectGetWidth(cupBac), CGRectGetMinY(cupBac) + 0.22674 * CGRectGetHeight(cupBac))];
        [cupBack2Path addCurveToPoint: CGPointMake(CGRectGetMinX(cupBac) + 0.94554 * CGRectGetWidth(cupBac), CGRectGetMinY(cupBac) + 0.29023 * CGRectGetHeight(cupBac)) controlPoint1: CGPointMake(CGRectGetMinX(cupBac) + 0.74607 * CGRectGetWidth(cupBac), CGRectGetMinY(cupBac) + 0.22674 * CGRectGetHeight(cupBac)) controlPoint2: CGPointMake(CGRectGetMinX(cupBac) + 0.94554 * CGRectGetWidth(cupBac), CGRectGetMinY(cupBac) + 0.25517 * CGRectGetHeight(cupBac))];
        [cupBack2Path addLineToPoint: CGPointMake(CGRectGetMinX(cupBac) + 0.63366 * CGRectGetWidth(cupBac), CGRectGetMinY(cupBac) + 0.29023 * CGRectGetHeight(cupBac))];
        [cupBack2Path closePath];
        cupBack2Path.miterLimit = 6;
        
        CGContextSaveGState(context);
        CGContextSetShadowWithColor(context, shadow2Offset, shadow2BlurRadius, shadow2.CGColor);
        CGContextBeginTransparencyLayer(context, NULL);
        [cupBack2Path addClip];
        CGRect cupBack2Bounds = CGPathGetPathBoundingBox(cupBack2Path.CGPath);
        CGContextDrawLinearGradient(context, waterGradient,
                                    CGPointMake(CGRectGetMinX(cupBack2Bounds), CGRectGetMidY(cupBack2Bounds)),
                                    CGPointMake(CGRectGetMaxX(cupBack2Bounds), CGRectGetMidY(cupBack2Bounds)),
                                    0);
        CGContextEndTransparencyLayer(context);
        CGContextRestoreGState(context);
        
    }
    
    
    //// Cleanup
    CGGradientRelease(waterGradient);
    CGGradientRelease(glassGradient);
    CGColorSpaceRelease(colorSpace);
}

@end
