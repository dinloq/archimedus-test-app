//
//  AcuariumView.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 24.02.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "AcuariumView.h"
#import "BucketView.h"
#import "BucketBackView.h"

@interface AcuariumView()
@property (strong, nonatomic) UIDynamicAnimator *animator;
@property (strong, nonatomic) BucketView *bucket;
@property (strong, nonatomic) BucketBackView *bucketBack;
@property (strong, nonatomic) UIView *cube;
@property (nonatomic) float boundSize;

@end

@implementation AcuariumView

@synthesize boundSize;

# pragma mark Animation

-(void)animateCubeFloat:(BOOL)willFloat
{
    [self willAnimate];
    
    // Falling animation
    [UIView animateWithDuration:1.4 delay:0 options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         CGRect frame = CGRectMake(self.bounds.size.width / 2 - (boundSize / 2),
                                                   0 + boundSize / 4,
                                                   boundSize, boundSize);
                         self.cube.frame = frame;
                     }
                     completion:^(BOOL finished){
                         if(willFloat) [self floatAnimation];
                         else      [self drownAnimation];
                     }];
}

-(void)willAnimate
{
    [self removeCube];
    [self insertSubview:self.cube atIndex:1];
}

-(void)drownAnimation
{
    [UIView animateWithDuration:2
                     animations:^
    {
        CGRect frame = CGRectMake(self.bounds.size.width / 2 - (boundSize / 2),
                                    self.bounds.size.height - (boundSize * 1.2),
                                    boundSize, boundSize);
        [self.cube setFrame:frame];
    }];
}

-(void)floatAnimation
{
    [UIView animateWithDuration:3
                          delay:0.5
                        options:UIViewAnimationOptionAutoreverse
                     animations:^
     {
         CGRect frame = CGRectMake(self.bounds.size.width / 2 - (boundSize / 2),
                                   boundSize / 2, boundSize, boundSize);
         [self.cube setFrame:frame];
     } completion:nil];
}

#pragma mark Initialization

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

-(void)setup
{
    self.backgroundColor = nil;
    self.opaque = NO;
}

-(void)awakeFromNib
{
    [self setup];
    self.boundSize = self.frame.size.width / 4;
}

#pragma mark Drawing

-(UIView *)cube
{
    if(!_cube) {
        CGRect frame = CGRectMake(self.bounds.size.width / 2  - (boundSize / 2), 0 - self.bounds.size.height,
                                  boundSize, boundSize);
        CGRect imageFrame = CGRectMake(0, 0,
                                       boundSize, boundSize / 5 * 4);
    
        UIImage *backgroundImage = [UIImage imageNamed:@"sugar"];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:imageFrame];
        [imageView setImage:[backgroundImage stretchableImageWithLeftCapWidth:3 topCapHeight:3]];
    
        UIView *view = [[UIImageView alloc] initWithFrame:frame];
        [view addSubview:imageView];
        _cube = view;
    }
    return _cube;
}

-(void)removeCube
{
    [self.cube removeFromSuperview];
    self.cube = nil;
}

-(BucketView *)bucket
{
    if (!_bucket) _bucket = [[BucketView alloc] initWithFrame:self.bounds];
    return _bucket;
}

-(BucketBackView *)bucketBack
{
    if (!_bucketBack) _bucketBack = [[BucketBackView alloc] initWithFrame:self.bounds];
    return _bucketBack;
}

-(void)drawRect:(CGRect)rect
{
    [self drawBucket];
    [self drawBucketBack];
}

-(void)drawBucket
{
    [self insertSubview:self.bucket atIndex:2];
}

-(void)drawBucketBack
{
    [self insertSubview:self.bucketBack atIndex:0];
}

@end
