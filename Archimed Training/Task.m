//
//  Task.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 20.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "Task.h"

@implementation Task

-(id)initWithBody:(Material *)bodyMaterial liquid:(Material *)liquidMaterial
{
    self = [super init];
    
    if (self) {
        self.body = bodyMaterial;
        self.liquid = liquidMaterial;
    }
    
    return self;
}

-(id)init DEPRECATED_ATTRIBUTE
{
    return nil;
}

#pragma mark - Setters

-(BOOL)willFloat
{
    if (!_willFloat) _willFloat = self.body.density.integerValue < self.liquid.density.integerValue;
    return _willFloat;
}

-(float)mass
{
    if (!_mass || _mass == 0) _mass = 10 + (arc4random() % 200) / 100;
    return _mass;
}

-(NSAttributedString *)description
{
    if (!_description) _description = [self createDescription];
    return _description;
}

-(NSAttributedString *)createDescription
{
    NSMutableAttributedString *result = [[NSMutableAttributedString alloc] init];
    
    [result appendAttributedString:[[NSAttributedString alloc] initWithString:@"Твердое тело "]];
    
    NSMutableAttributedString *temp = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"(%@)", _body.name]];
    [self foregroundColorForAttributedString:temp];
    [result appendAttributedString:temp];
    
    [result appendAttributedString:[[NSAttributedString alloc] initWithString:@" с удельной плотностью "]];
    
    temp = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%ld кг/м3", (long)self.body.density.integerValue]];
    [self foregroundColorForAttributedString:temp];
    [result appendAttributedString:temp];
    
    [result appendAttributedString:[[NSAttributedString alloc] initWithString:@" и массой "]];
    
    temp = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%.2f кг", self.mass]];
    [self foregroundColorForAttributedString:temp];
    [result appendAttributedString:temp];

    [result appendAttributedString:[[NSAttributedString alloc] initWithString:@" погружено в жидкость "]];
    
    temp = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"(%@)", self.liquid.name]];
    [self foregroundColorForAttributedString:temp];
    [result appendAttributedString:temp];
    
    [result appendAttributedString:[[NSAttributedString alloc] initWithString:@" с удельной плотностью "]];
    
    temp = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%ld кг/м3", (long)self.liquid.density.integerValue]];
    [self foregroundColorForAttributedString:temp];
    [result appendAttributedString:temp];
    
    [result appendAttributedString:[[NSAttributedString alloc] initWithString:@". Вспылвет ли тело или опустится на дно?"]];
    
    return result;
}

-(void)foregroundColorForAttributedString:(NSMutableAttributedString *)string
{
    [string addAttribute:NSForegroundColorAttributeName
                 value:MAIN_COLOR
                 range:NSMakeRange(0, string.length)];
}


-(NSAttributedString *)initialProplemTextWithBody:(Material *)body bodyVolume:(float)volume liquid:(Material *)liquid
{
    NSString *string = [NSString stringWithFormat:@"Твердое тело (%@) с удельной плотностью %@ кг/м3 и массой %.2f кг погружено в жидкость (%@) с удельной плотностью %@ кг/м3. Вспылвет ли тело или опустится на дно?", body.name, body.density, volume, liquid.name, liquid.density ];
    //Твердое тело (Железо) с удельной плотностью 700 кг/м3 и объемом 0.1 л. погружено в жидкость (Вода) с удельной плотностью 1000 кг/м3. Вспылвет ли тело или опустится на дно?
    
    return [[NSAttributedString alloc] initWithString:string];
}


@end
