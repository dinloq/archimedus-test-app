//
//  SelectableSearchViewController.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 23.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utils/ArchUtils.h"

@interface AbstractSelectableSearchViewController : UITableViewController <UISearchDisplayDelegate, UISearchBarDelegate>
@property (strong, nonatomic) NSMutableArray *data;
@property (strong, nonatomic) NSManagedObject *entity;
-(void)throwError;
@end
