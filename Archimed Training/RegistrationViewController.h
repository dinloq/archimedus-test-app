//
//  RegistrationViewController.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 20.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreData/Student.h"

@interface RegistrationViewController : UITableViewController <UIPickerViewDelegate, UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UIPickerView *gradePicker;
@property (strong, nonatomic) Student *student;
@end
