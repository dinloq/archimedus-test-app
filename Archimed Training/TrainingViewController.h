//
//  TrainingViewController.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 20.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreData/CoreDataAccessor.h"
#import "CoreData/Student.h"
#import "Utils/ArchUtils.h"

@interface TrainingViewController : UIViewController <HasManagedObjectField>
@end
