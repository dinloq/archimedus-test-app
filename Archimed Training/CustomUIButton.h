//
//  CustomUIButton.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 02.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomUIButton : UIButton {
    CAGradientLayer *shineLayer;
    CALayer *highlightLayer;
}
@end
