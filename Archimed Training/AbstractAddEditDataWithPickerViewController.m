//
//  AbstractAddEditDataWithPickerViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 27.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "AbstractAddEditDataWithPickerViewController.h"
#import "CoreDataAccessor.h"

@interface AbstractAddEditDataWithPickerViewController ()
@property (strong, nonatomic) NSArray *grades;
@property (weak, nonatomic) IBOutlet UIPickerView *gradesPicker;
@end

@implementation AbstractAddEditDataWithPickerViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.gradesPicker setDelegate:self];
    [self.gradesPicker setDataSource:self];
}

-(void)viewWillAppear:(BOOL)animated
{
    Grade *grade = [self getEntityGrade];
    if (grade) {
        [self selectPickerWithGrade:grade];
    }
}

-(NSArray *)grades
{
    if (!_grades) {
        _grades = [CoreDataAccessor getAllEntities:@"Grade"];
    }
    return _grades;
}

#pragma mark - Picker metods

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.grades count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    Grade *grade = [self.grades objectAtIndex:row];
    return grade.classname;
}

-(Grade *)getSelectedGrade
{
    NSUInteger index = [self.gradesPicker selectedRowInComponent:0];
    return [self.grades objectAtIndex:index];
}

-(void)selectPickerWithGrade:(Grade *)grade
{
    NSUInteger index = [self.grades indexOfObject:grade];
    [self.gradesPicker selectRow:index inComponent:0 animated:YES];
}

#pragma mark - Abstract method

-(Grade *)getEntityGrade
{
    return nil;
}

@end
