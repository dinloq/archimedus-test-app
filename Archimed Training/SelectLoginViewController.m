//
//  SelectLoginViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 01.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "SelectLoginViewController.h"
#import "LoginPageViewController.h"

@interface SelectLoginViewController ()

@end

@implementation SelectLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if (![segue.destinationViewController isKindOfClass:[LoginPageViewController class]]) {
        return;
    }
    LoginPageViewController *view = (LoginPageViewController *) segue.destinationViewController;
    if ([segue.identifier isEqualToString:@"Login Teacher"]) {
        view.isTeacher = YES;
    } else if ([segue.identifier isEqualToString:@"Login Student"]) {
        view.isTeacher = NO;
    }
}

@end
