//
//  StudentMenuViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 20.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "StudentMenuViewController.h"
#import "CoreData/Student.h"
#import "TrainingViewController.h"

@interface StudentMenuViewController ()
@property (strong,nonatomic) Student *student;
@end

@implementation StudentMenuViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.title = [NSString stringWithFormat:@"%@ %@", _student.firstname, _student.secondname];
}

-(void)setManagedObject:(NSManagedObject *)object
{
    if ([object isKindOfClass:[Student class]]) {
        self.student = (Student *) object;
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController conformsToProtocol:@protocol(HasManagedObjectField)]) {
        UIViewController<HasManagedObjectField> *controller = segue.destinationViewController;
        [controller setManagedObject:self.student];
    }
}

@end
