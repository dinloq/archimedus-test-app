//
//  BucketView.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 26.02.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BucketView : UIView
@property (nonatomic) NSInteger textureOffset;

-(void)incTextureOffset;

@end
