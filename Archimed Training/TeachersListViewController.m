//
//  TeachersListViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 28.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "TeachersListViewController.h"
#import "Teacher.h"
#import "CoreDataAccessor.h"
#import "Grade.h"
#import "MessagerViewController.h"

@interface TeachersListViewController ()
@property (strong, nonatomic) Student *student;
@end

@implementation TeachersListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    MessagerViewController *vc = segue.destinationViewController;
    
    [vc initWithTeacher:(Teacher *)self.entity
                student:self.student
              isTeacher:NO];
}

#pragma mark - Override methods

-(void)setManagedObject:(NSManagedObject *)object
{
    self.student = (Student *)object;
}

-(NSMutableArray *)loadData
{
    return [NSMutableArray arrayWithArray:[CoreDataAccessor getAllEntities:@"Teacher"]];
}

-(void)configureCell:(UITableViewCell *)cell withManagedObject:(NSManagedObject *)object
{
    Teacher *teacher = (Teacher *)object;
    
    cell.textLabel.text = teacher.firstname;
    cell.detailTextLabel.text = teacher.grade.classname;
}

-(NSString *)segueIdentifier
{
    return @"Messager";
}

-(NSString *)cellIdentifier
{
    return @"Cell";
}

-(NSString *)predicateString
{
    return @"SELF.firstname contains[c] $SEARCH OR SELF.secondname contains[c] $SEARCH";
}

@end
