//
//  HMIAppDelegate.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 24.02.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HMIAppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@end