//
//  DisplayGuideSectionViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 22.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "DisplayGuideSectionViewController.h"
#import "CoreData/Guide.h"

@interface DisplayGuideSectionViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, strong) Guide *content;
@end

@implementation DisplayGuideSectionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.webView loadHTMLString:self.content.text baseURL:nil];
}

-(void)setManagedObject:(NSManagedObject *)object
{
    self.content = (Guide *) object;
}

@end
