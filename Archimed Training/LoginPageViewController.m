//
//  LoginPageViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 02.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "LoginPageViewController.h"
#import "CoreData/CoreDataAccessor.h"
#import "CoreData/Student.h"
#import "CoreData/Teacher.h"

@interface LoginPageViewController ()
@property (weak, nonatomic) IBOutlet UITableViewCell *registrationCell;
@property (weak, nonatomic) IBOutlet UITextField *loginField;
@property (nonatomic) BOOL isLoginFailed;
@property (nonatomic, strong) NSManagedObject *authorizedUser;
@end

@implementation LoginPageViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
        // Custom initialization
    }
    return self;
}
- (IBAction)doneEditing:(id)sender
{
    [self.loginField resignFirstResponder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.registrationCell.hidden = self.isTeacher;
}

#pragma mark - Segues login

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController conformsToProtocol:@protocol(HasManagedObjectField)]) {
        UIViewController<HasManagedObjectField> *controller = segue.destinationViewController;
        [controller setManagedObject:self.authorizedUser];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1 && indexPath.section == 0)
    {
        [self verifyUserAndLogin];
    }
}

#pragma mark - Display error

-(NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    switch (section) {
        case 1:
            return self.isLoginFailed ? @"Не корректные данные" : nil;
            break;
        default:
            return nil;
            break;
    }
}

#pragma mark - Login and Authorization Logic

-(void)verifyUserAndLogin
{
    NSString *entityName = (self.isTeacher) ? @"Teacher" : @"Student";
    NSManagedObject *entity = [self getEntity:entityName
                                      byLogin:self.loginField.text];
    // validate existing entity in core data
    if (!entity) {
        self.isLoginFailed = YES;
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1]
                      withRowAnimation:UITableViewRowAnimationFade];
        return;
    }
    self.authorizedUser = entity;
    [self performSegueWithIdentifier:entityName sender:self];
}

-(NSManagedObject *)getEntity:(NSString *)entity byLogin:(NSString *)login
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"login = %@", login];
    NSArray *array = [CoreDataAccessor getAllEntities:entity
                                           filterWith:predicate];
    
    return ([array count]) ? [array objectAtIndex:0] : nil;
}

@end
