//
//  DisplayGuideSectionViewController.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 22.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utils/ArchUtils.h"

@interface DisplayGuideSectionViewController : UIViewController <HasManagedObjectField>
@end
