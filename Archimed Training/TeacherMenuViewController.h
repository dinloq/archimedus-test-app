//
//  TeacherMenuViewController.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 23.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utils/ArchUtils.h"

@interface TeacherMenuViewController : UITableViewController <HasManagedObjectField>
@end
