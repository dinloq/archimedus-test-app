//
//  AbstractEditableListViewController.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 23.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "AbstractSelectableSearchViewController.h"

@interface AbstractEditableListViewController : AbstractSelectableSearchViewController

@end
