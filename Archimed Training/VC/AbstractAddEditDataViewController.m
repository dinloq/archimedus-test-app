//
//  AbstractAddEditDataViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 23.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "AbstractAddEditDataViewController.h"
#import "CoreDataAccessor.h"

@interface AbstractAddEditDataViewController ()
@end

@implementation AbstractAddEditDataViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addDoneButtonToView];
    [self initFields];
}

-(void)addDoneButtonToView
{
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                                                                target:self
                                                                                action:@selector(saveManagedObject)];
    self.navigationItem.rightBarButtonItem = doneButton;
}

-(void)setManagedObject:(NSManagedObject *)object
{
    self.entity = object;
}

-(void)saveManagedObject
{
    if (_entity) {
        [self updateItem];
    } else {
        [self insertItem];
    }
    [[self navigationController] popViewControllerAnimated:YES];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (![self isSaveRowIndexPath:indexPath]) return;
    [self saveManagedObject];
}

#pragma mark - Insert and Update logic

-(void)updateItem
{
    [self setEntityAttributes:_entity];
    NSError *error;
    if (![[CoreDataAccessor managedContext] save:&error]) {
        NSLog(@"Something wrong, couldn't save: + %@", [error localizedDescription]);
    }
}

-(void)insertItem
{
    _entity = [NSEntityDescription insertNewObjectForEntityForName:[self getEntityName]
                                              inManagedObjectContext:[CoreDataAccessor managedContext]];
    [self updateItem];
}

#pragma mark - TextField returner

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder *nextResponder = [self.view viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

#pragma mark - Abstract methods
// -----------------------------
// methods needs to be overrided
// -----------------------------

-(BOOL)isSaveRowIndexPath:(NSIndexPath *)indexPath
{
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
    return YES;
}

-(void)initFields
{
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
}

-(NSString *)getEntityName
{
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
    return nil;
}

-(void)setEntityAttributes:(NSManagedObject *)entity
{
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
}


@end
