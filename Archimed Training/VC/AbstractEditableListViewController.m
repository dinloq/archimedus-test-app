//
//  AbstractEditableListViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 23.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "AbstractEditableListViewController.h"
#import "../CoreData/CoreDataAccessor.h"

@interface AbstractEditableListViewController ()

@end

@implementation AbstractEditableListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewItem)];
    self.navigationItem.rightBarButtonItem = addButton;
    // self.navigationItem.rightBarButtonItems = @[addButton, self.editButtonItem];
}

-(void)addNewItem
{
    self.entity = nil;
    [self performSegueWithIdentifier:[self segueIdentifier] sender:self];
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [CoreDataAccessor removeObjects:@[[self.data objectAtIndex:indexPath.row]]];
        [self.data removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                         withRowAnimation:UITableViewRowAnimationFade];
        
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        
    }
}

///// ABSTRACT

-(NSMutableArray *)loadData
{
    [self throwError];
    return nil;
}


-(void)configureCell:(UITableViewCell *)cell withManagedObject:(NSManagedObject *)object
{
    [self throwError];
}

-(NSString *)segueIdentifier
{
    [self throwError];
    return nil;
}

-(NSString *)cellIdentifier
{
    [self throwError];
    return nil;
}

-(NSString *)predicateString
{
    [self throwError];
    return nil;
}

@end
