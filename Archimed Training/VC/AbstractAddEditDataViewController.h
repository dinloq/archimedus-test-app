//
//  AbstractAddEditDataViewController.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 23.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../Utils/ArchUtils.h"

@interface AbstractAddEditDataViewController : UITableViewController <HasManagedObjectField, UITextFieldDelegate, UITextViewDelegate>
@property (strong, nonatomic) NSManagedObject *entity;
@end
