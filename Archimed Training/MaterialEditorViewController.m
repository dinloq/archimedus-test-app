//
//  MaterialEditorViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 23.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "MaterialEditorViewController.h"
#import "CoreData/Material.h"
#import "CoreDataAccessor.h"

@interface MaterialEditorViewController ()
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation MaterialEditorViewController

#pragma mark - Abstract Methods

-(NSMutableArray *)loadData
{
    NSSortDescriptor *nameSort = [NSSortDescriptor sortDescriptorWithKey:@"name"
                                                                   ascending:YES];
    return [[NSMutableArray alloc] initWithArray:[CoreDataAccessor getAllEntities:@"Material"
                              withPredicate:nil
                                   withSort:@[nameSort]]];
}

-(Material *)convertToStudent:(NSManagedObject *)object
{
    return (Material *) object;
}

-(void)configureCell:(UITableViewCell *)cell withManagedObject:(NSManagedObject *)object
{
    Material *material = [self convertToStudent:object];
    
    cell.detailTextLabel.text = material.density.stringValue;
    cell.textLabel.text = material.name;
}

-(NSString *)segueIdentifier
{
    return @"AddEdit";
}

-(NSString *)cellIdentifier
{
    return @"Cell";
}

-(NSString *)predicateString
{
    return @"SELF.name contains[c] $SEARCH";
}

@end
