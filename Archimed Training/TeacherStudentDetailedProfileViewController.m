//
//  TeacherStudentDetailedProfileViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 29.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "TeacherStudentDetailedProfileViewController.h"
#import "MessagerViewController.h"

@interface TeacherStudentDetailedProfileViewController ()

@property (weak, nonatomic) IBOutlet UITableViewCell *nameCell;

@property (weak, nonatomic) IBOutlet UITableViewCell *gradeCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *loginCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *rightCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *wrongCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *ratingCell;

@property (strong, nonatomic) Teacher *teacher;
@end

@implementation TeacherStudentDetailedProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setTeacherObject:(Teacher *)teacher
{
    self.teacher = teacher;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    MessagerViewController *vc = segue.destinationViewController;
    [vc initWithTeacher:self.teacher student:self.student isTeacher:YES];
}


@end
