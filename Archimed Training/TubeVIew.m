//
//  TubeVIew.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 01.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "TubeVIew.h"

@implementation TubeVIew

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

-(void)setup
{
    self.backgroundColor = nil;
    self.opaque = NO;
}

-(void)awakeFromNib
{
    [self setup];
}

- (void)drawRect:(CGRect)rect
{
    //// Color Declarations
    UIColor* color = [UIColor colorWithRed: 0.333 green: 0.333 blue: 0.333 alpha: 1];
    UIColor* color2 = [UIColor colorWithRed: 0.5 green: 0.5 blue: 0.5 alpha: 1];
    UIColor* color3 = [UIColor colorWithRed: 0.667 green: 0.667 blue: 0.667 alpha: 1];
    
    //// Frames
    CGRect frame = self.bounds;
    
    
    //// Bezier 3 Drawing
    UIBezierPath* bezier3Path = [UIBezierPath bezierPath];
    [bezier3Path moveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.02885 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.85606 * CGRectGetHeight(frame))];
    [bezier3Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.22115 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.94697 * CGRectGetHeight(frame))];
    [bezier3Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.27885 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.55303 * CGRectGetHeight(frame))];
    [bezier3Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.29808 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.08333 * CGRectGetHeight(frame))];
    [bezier3Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.20192 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.06818 * CGRectGetHeight(frame))];
    [bezier3Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.12500 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.52273 * CGRectGetHeight(frame))];
    [bezier3Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.02885 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.85606 * CGRectGetHeight(frame))];
    [bezier3Path closePath];
    [color setFill];
    [bezier3Path fill];
    [[UIColor blackColor] setStroke];
    bezier3Path.lineWidth = 1;
    [bezier3Path stroke];
    
    
    //// Bezier 4 Drawing
    UIBezierPath* bezier4Path = [UIBezierPath bezierPath];
    [bezier4Path moveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.29808 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.08333 * CGRectGetHeight(frame))];
    [bezier4Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.70192 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.08333 * CGRectGetHeight(frame))];
    [bezier4Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.72115 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.55303 * CGRectGetHeight(frame))];
    [bezier4Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.81731 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.94697 * CGRectGetHeight(frame))];
    [bezier4Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.52885 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.96212 * CGRectGetHeight(frame))];
    [bezier4Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.22115 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.94697 * CGRectGetHeight(frame))];
    [bezier4Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.27885 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.52273 * CGRectGetHeight(frame))];
    [bezier4Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.29808 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.08333 * CGRectGetHeight(frame))];
    [bezier4Path closePath];
    [color2 setFill];
    [bezier4Path fill];
    [[UIColor blackColor] setStroke];
    bezier4Path.lineWidth = 1;
    [bezier4Path stroke];
    
    
    //// Bezier 5 Drawing
    UIBezierPath* bezier5Path = [UIBezierPath bezierPath];
    [bezier5Path moveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.70192 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.08333 * CGRectGetHeight(frame))];
    [bezier5Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.81731 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.06818 * CGRectGetHeight(frame))];
    [bezier5Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.85577 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.50758 * CGRectGetHeight(frame))];
    [bezier5Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.97115 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.85606 * CGRectGetHeight(frame))];
    [bezier5Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.81731 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.94697 * CGRectGetHeight(frame))];
    [bezier5Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.72115 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.55303 * CGRectGetHeight(frame))];
    [bezier5Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.70192 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.08333 * CGRectGetHeight(frame))];
    [bezier5Path closePath];
    [color3 setFill];
    [bezier5Path fill];
    [[UIColor blackColor] setStroke];
    bezier5Path.lineWidth = 1;
    [bezier5Path stroke];
    
    
    //// Bezier Drawing
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.79808 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.94697 * CGRectGetHeight(frame))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.50000 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.96970 * CGRectGetHeight(frame))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.20192 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.94697 * CGRectGetHeight(frame))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.02885 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.85606 * CGRectGetHeight(frame))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.12500 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.51619 * CGRectGetHeight(frame))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.20192 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.06818 * CGRectGetHeight(frame))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.32692 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.08333 * CGRectGetHeight(frame))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.62500 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.08333 * CGRectGetHeight(frame))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.79808 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.06818 * CGRectGetHeight(frame))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.87500 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.51619 * CGRectGetHeight(frame))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.97115 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.85606 * CGRectGetHeight(frame))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.79808 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.94697 * CGRectGetHeight(frame))];
    [bezierPath closePath];
    [[UIColor blackColor] setStroke];
    bezierPath.lineWidth = 1.5;
    [bezierPath stroke];
    
    
    //// Bezier 2 Drawing
    UIBezierPath* bezier2Path = [UIBezierPath bezierPath];
    [bezier2Path moveToPoint: CGPointMake(CGRectGetMinX(frame) + 0.12500 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.51515 * CGRectGetHeight(frame))];
    [bezier2Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.28846 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.55303 * CGRectGetHeight(frame))];
    [bezier2Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.48077 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.56818 * CGRectGetHeight(frame))];
    [bezier2Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.66346 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.55303 * CGRectGetHeight(frame))];
    [bezier2Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 0.87500 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.51515 * CGRectGetHeight(frame))];
    [[UIColor blackColor] setStroke];
    bezier2Path.lineWidth = 1;
    [bezier2Path stroke];
}


@end
