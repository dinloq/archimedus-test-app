//
//  ArchNavigationViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 02.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "ArchNavigationViewController.h"

@interface ArchNavigationViewController ()

@end

@implementation ArchNavigationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

}

@end
