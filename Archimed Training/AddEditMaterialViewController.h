//
//  AddEditMaterialViewController.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 23.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utils/ArchUtils.h"
#import "VC/AbstractAddEditDataViewController.h"

@interface AddEditMaterialViewController : AbstractAddEditDataViewController
@end
