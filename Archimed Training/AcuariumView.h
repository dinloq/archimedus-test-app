//
//  AcuariumView.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 24.02.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AcuariumView : UIView
-(void)animateCubeFloat:(BOOL)willFloat;
-(void)removeCube;
@end
