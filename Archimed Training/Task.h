//
//  Task.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 20.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoreData/Material.h"

@interface Task : NSObject

@property (nonatomic) BOOL willFloat;
@property (nonatomic, strong) Material *body;
@property (nonatomic, strong) Material *liquid;
@property (nonatomic) float mass;
@property (nonatomic, strong) NSAttributedString *description;

-(id)initWithBody:(Material *)bodyMaterial liquid:(Material *)liquidMaterial;

@end
