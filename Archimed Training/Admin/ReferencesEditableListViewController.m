//
//  ReferencesEditableListViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 26.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "ReferencesEditableListViewController.h"
#import "CoreDataAccessor.h"
#import "Guide.h"

@interface ReferencesEditableListViewController ()

@end

@implementation ReferencesEditableListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark - Abstract methods

-(NSMutableArray *)loadData
{
    return [[NSMutableArray alloc] initWithArray:[CoreDataAccessor getAllEntities:@"Guide"]];
}

-(void)configureCell:(UITableViewCell *)cell withManagedObject:(NSManagedObject *)object
{
    Guide *guide = (Guide *) object;
    
    cell.textLabel.text = guide.section;
}

-(NSString *)segueIdentifier
{
    return @"EditReference";
}

-(NSString *)cellIdentifier
{
    return @"Cell";
}

-(NSString *)predicateString
{
    return @"SELF.section contains[c] $SEARCH OR SELF.text contains[c] $SEARCH";
}

@end
