//
//  AddEditTeacherViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 26.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "AddEditTeacherViewController.h"
#import "../CoreData/Teacher.h"
#import "../CoreData/Grade.h"
#import "CoreDataAccessor.h"

@interface AddEditTeacherViewController ()
@property (weak, nonatomic) IBOutlet UITextField *firstNameTF;
@property (weak, nonatomic) IBOutlet UITextField *secondNameTF;
@property (weak, nonatomic) IBOutlet UITextField *loginTF;
@property (weak, nonatomic) IBOutlet UIPickerView *gradesPicker;

@end

@implementation AddEditTeacherViewController

#pragma mark - Abstract implementation

-(Grade *)getEntityGrade
{
    return ((Teacher *)self.entity).grade;
}

-(BOOL)isSaveRowIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

-(void)initFields
{
    [self.firstNameTF setDelegate:self];
    [self.secondNameTF setDelegate:self];
    [self.loginTF setDelegate:self];
    
    Teacher *teacher = (Teacher *) self.entity;
    if (teacher) {
        [self.firstNameTF setText:teacher.firstname];
        [self.secondNameTF setText:teacher.secondname];
        [self.loginTF setText:teacher.login];
    }
}

-(NSString *)getEntityName
{
    return @"Teacher";
}

-(void)setEntityAttributes:(NSManagedObject *)entity
{
    Teacher *teacher = (Teacher *) self.entity;
    teacher.firstname = self.firstNameTF.text;
    teacher.secondname = self.secondNameTF.text;
    teacher.login = self.loginTF.text;

    teacher.grade = [self getSelectedGrade];
}

@end
