//
//  AddEditTeacherViewController.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 26.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "AbstractAddEditDataWithPickerViewController.h"

@interface AddEditTeacherViewController : AbstractAddEditDataWithPickerViewController

@end
