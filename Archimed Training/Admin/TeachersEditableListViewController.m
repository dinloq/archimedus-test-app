//
//  TeachersEditableListViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 26.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "TeachersEditableListViewController.h"
#import "CoreDataAccessor.h"
#import "../CoreData/Teacher.h"
#import "../CoreData/Grade.h"

@interface TeachersEditableListViewController ()

@end

@implementation TeachersEditableListViewController

#pragma mark - Abstract methods

-(NSMutableArray *)loadData
{
    return [[NSMutableArray alloc] initWithArray:[CoreDataAccessor getAllEntities:@"Teacher"]];
}

-(void)configureCell:(UITableViewCell *)cell withManagedObject:(NSManagedObject *)object
{
    Teacher *teacher = (Teacher *) object;
    
    cell.textLabel.text = teacher.firstname;
    cell.detailTextLabel.text = teacher.grade.classname;
}

-(NSString *)segueIdentifier
{
    return @"EditTeacher";
}

-(NSString *)cellIdentifier
{
    return @"Cell";
}

-(NSString *)predicateString
{
    return @"SELF.firstname contains[c] $SEARCH OR SELF.secontname contains[c] $SEARCH";
}

@end
