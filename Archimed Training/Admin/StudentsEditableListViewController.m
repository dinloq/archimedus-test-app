//
//  StudentsEditableListViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 26.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "StudentsEditableListViewController.h"
#import "CoreDataAccessor.h"
#import "../CoreData/Student.h"
#import "../CoreData/Grade.h"

@interface StudentsEditableListViewController ()

@end

@implementation StudentsEditableListViewController

#pragma mark - Abstract methods

-(NSMutableArray *)loadData
{
    return [[NSMutableArray alloc] initWithArray:[CoreDataAccessor getAllEntities:@"Student"]];
}

-(void)configureCell:(UITableViewCell *)cell withManagedObject:(NSManagedObject *)object
{
    Student *student = (Student *)object;
    
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ %@", student.firstname, student.secondname];
    cell.textLabel.text = student.grade.classname;
}

-(NSString *)segueIdentifier
{
    return @"EditStudent";
}

-(NSString *)cellIdentifier
{
    return @"Cell";
}

-(NSString *)predicateString
{
    return @"SELF.firstname contains[c] $SEARCH OR SELF.secondname contains[c] $SEARCH";
}

@end
