//
//  AddEditClassViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 26.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "AddEditClassViewController.h"
#import "CoreDataAccessor.h"
#import "../CoreData/Grade.h"

@interface AddEditClassViewController ()
@property (weak, nonatomic) IBOutlet UITextField *classTF;
@end

@implementation AddEditClassViewController

#pragma mark - Abstract implementation

-(BOOL)isSaveRowIndexPath:(NSIndexPath *)indexPath
{
    return (indexPath.row == 0 && indexPath.section == 1) ? YES : NO;
}

-(void)initFields
{
    Grade *grade = (Grade *)self.entity;
    if (grade) {
        self.classTF.text = grade.classname;
    }
}

-(NSString *)getEntityName
{
    return @"Grade";
}

-(void)setEntityAttributes:(NSManagedObject *)entity
{
    Grade *grade = (Grade *)self.entity;
    grade.classname = self.classTF.text;
}

@end
