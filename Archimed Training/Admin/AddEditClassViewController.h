//
//  AddEditClassViewController.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 26.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "AbstractAddEditDataViewController.h"

@interface AddEditClassViewController : AbstractAddEditDataViewController

@end
