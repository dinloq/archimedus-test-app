//
//  ReferenceAddEditViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 26.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "AddEditReferenceViewController.h"
#import "../CoreData/Guide.h"

@interface AddEditReferenceViewController ()
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITextView *textField;
@end

@implementation AddEditReferenceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark - Abstract implementation

-(BOOL)isSaveRowIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

-(void)initFields
{
    [self.titleTextField setDelegate:self];
    [self.textField setDelegate:self];
    
    Guide *guide = (Guide *) self.entity;
    if (guide) {
        self.titleTextField.text = guide.section;
        [self.textField setAttributedText:[[NSAttributedString alloc] initWithString:guide.text]];
    }
}

-(NSString *)getEntityName
{
    return @"Guide";
}

-(void)setEntityAttributes:(NSManagedObject *)entity
{
    Guide *guide = (Guide *)entity;
    guide.section = self.titleTextField.text;
    guide.text = self.textField.attributedText.string;
}

@end
