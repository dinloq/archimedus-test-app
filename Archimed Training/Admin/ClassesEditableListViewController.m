//
//  ClassesEditableListViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 26.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "ClassesEditableListViewController.h"
#import "../CoreData/Grade.h"
#import "CoreDataAccessor.h"

@interface ClassesEditableListViewController ()

@end

@implementation ClassesEditableListViewController

#pragma mark - Abstract methods

-(NSMutableArray *)loadData
{
    return [[NSMutableArray alloc] initWithArray:[CoreDataAccessor getAllEntities:@"Grade"]];
}

-(void)configureCell:(UITableViewCell *)cell withManagedObject:(NSManagedObject *)object
{
    Grade *grade = (Grade *) object;
    
    cell.textLabel.text = grade.classname;
}

-(NSString *)segueIdentifier
{
    return @"EditGrade";
}

-(NSString *)cellIdentifier
{
    return @"Cell";
}

-(NSString *)predicateString
{
    return @"SELF.classname contains[c] $SEARCH";
}

@end
