//
//  AddEditStudentViewController.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 26.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "AddEditStudentViewController.h"
#import "CoreDataAccessor.h"
#import "Student.h"
#import "Grade.h"

@interface AddEditStudentViewController ()
@property (weak, nonatomic) IBOutlet UITextField *firstNameTF;
@property (weak, nonatomic) IBOutlet UITextField *secondNameTF;
@property (weak, nonatomic) IBOutlet UITextField *loginTF;
@property (weak, nonatomic) IBOutlet UITextField *rightTF;
@property (weak, nonatomic) IBOutlet UITextField *wrongTF;
@property (weak, nonatomic) IBOutlet UIPickerView *gradesPicker;

@end

@implementation AddEditStudentViewController

#pragma mark - Abstract implementation

-(Grade *)getEntityGrade
{
    return ((Student *)self.entity).grade;
}

-(BOOL)isSaveRowIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

-(void)initFields
{
    Student *student = (Student *)self.entity;
    
    if (student) {
        self.firstNameTF.text = student.firstname;
        self.secondNameTF.text = student.secondname;
        self.loginTF.text = student.login;
        self.rightTF.text = student.right.stringValue;
        self.wrongTF.text = student.wrong.stringValue;
    }
}

-(NSString *)getEntityName
{
    return @"Student";
}

-(void)setEntityAttributes:(NSManagedObject *)entity
{
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    
    Student *student = (Student *)self.entity;
    student.firstname = self.firstNameTF.text;
    student.secondname = self.secondNameTF.text;
    student.login = self.loginTF.text;
    student.right = [f numberFromString:self.rightTF.text];
    student.wrong = [f numberFromString:self.wrongTF.text];
    student.grade = [self getSelectedGrade];
}

@end
