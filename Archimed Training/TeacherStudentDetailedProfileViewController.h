//
//  TeacherStudentDetailedProfileViewController.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 29.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "StudentProfileViewController.h"

@interface TeacherStudentDetailedProfileViewController : StudentProfileViewController

-(void)setTeacherObject:(Teacher *)teacher;
@end
