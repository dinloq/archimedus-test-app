//
//  UIFlatButton.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 20.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFlatButton : UIButton {
    CALayer *shineLayer;
    CALayer *highlightLayer;
}

@end
