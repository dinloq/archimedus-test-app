//
//  UIFlatButton.m
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 20.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "UIFlatButton.h"
#import "HMIAppDelegate.h"

@implementation UIFlatButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initLayers];
    }
    return self;
}

-(void)awakeFromNib
{
    [self initLayers];
}

-(void)initLayers
{
    [self initBorder];
    [self addShineLayer];
    [self addHighlightLayer];
    [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}

-(void)initBorder
{
    CALayer *layer = self.layer;
    layer.cornerRadius = 3.0f;
    layer.masksToBounds = YES;
    layer.borderWidth = 1.0f;
    layer.borderColor = [UIColor colorWithWhite:0.5f alpha:0.2f].CGColor;
}

-(void)addHighlightLayer
{
    highlightLayer = [CALayer layer];
    highlightLayer.backgroundColor = [UIColor colorWithRed:0.25f green:0.25f blue:0.25f alpha:0.75].CGColor;
    highlightLayer.frame = self.layer.bounds;
    highlightLayer.hidden = YES;
    [self.layer insertSublayer:highlightLayer below:shineLayer];
}

-(void)addShineLayer
{
    shineLayer = [CALayer layer];
    shineLayer.frame = self.layer.bounds;
    shineLayer.backgroundColor = [MAIN_COLOR colorWithAlphaComponent:0.7].CGColor;
    [self.layer addSublayer:shineLayer];
}

-(void)setHighlighted:(BOOL)highlighted
{
    highlightLayer.hidden = !highlighted;
    [super setHighlighted:highlighted];
}

-(void)setEnabled:(BOOL)enabled
{
    [super setEnabled:enabled];
    self.alpha = (enabled) ? 1 : 0.2;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
