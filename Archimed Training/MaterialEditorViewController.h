//
//  MaterialEditorViewController.h
//  Archimed Training
//
//  Created by Andrey Chernoprudov on 23.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "AbstractSelectableSearchViewController.h"
#import "AbstractEditableListViewController.h"

@interface MaterialEditorViewController : AbstractEditableListViewController
@end
