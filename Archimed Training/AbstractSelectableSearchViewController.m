//
//  SelectableSearchViewController.m
//  Archimed Training
//
//  Abstract class for Table View with dynamic search
//
//  Created by Andrey Chernoprudov on 23.03.14.
//  Copyright (c) 2014 Andrey Chernoprudov. All rights reserved.
//

#import "AbstractSelectableSearchViewController.h"

@interface AbstractSelectableSearchViewController ()
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *filteredData;
@end

@implementation AbstractSelectableSearchViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self hideSearchBar];
    self.filteredData = [[NSMutableArray alloc] initWithCapacity:[self.data count]];
    [self.tableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.data = nil;
    [self.tableView reloadData];
}

-(NSArray *)data
{
    if (!_data) _data = [self loadData];
    return _data;
}

#pragma mark - Search

-(void)hideSearchBar
{
    CGRect newBounds = self.tableView.bounds;
    newBounds.origin.y = newBounds.origin.y + self.searchBar.bounds.size.height;
    self.tableView.bounds = newBounds;
}

-(void)filterContentForSearchText:(NSString *)searchText scope:(NSString *)scope
{
    [self.filteredData removeAllObjects];
    NSPredicate *template = [NSPredicate predicateWithFormat:[self predicateString]];
    NSDictionary *replace = [NSDictionary dictionaryWithObject:searchText forKey:@"SEARCH"];
    NSPredicate *predicate = [template predicateWithSubstitutionVariables:replace];
    
    self.filteredData = [NSMutableArray arrayWithArray:[self.data filteredArrayUsingPredicate:predicate]];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    // Tells the table data source to reload when text changes
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                                         objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (tableView == self.searchDisplayController.searchResultsTableView)
        ? [self.filteredData count]
        : [self.data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:[self cellIdentifier] forIndexPath:indexPath];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:[self cellIdentifier]];
    }
    NSManagedObject *object = (tableView == self.searchDisplayController.searchResultsTableView)
        ? [self.filteredData objectAtIndex:indexPath.row]
        : [self.data objectAtIndex:indexPath.row];
    
    [self configureCell:cell withManagedObject:object];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.entity = (tableView == self.searchDisplayController.searchResultsTableView)
        ? [self.filteredData objectAtIndex:indexPath.row]
        : [self.data objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:[self segueIdentifier] sender:self];
}

#pragma mark - Segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController<HasManagedObjectField> *controller = segue.destinationViewController;
    [controller setManagedObject:self.entity];
}

-(void)throwError
{
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
}

#pragma mark - Abstract methods
// -----------------------------
// methods needs to be overrided
// -----------------------------

-(NSMutableArray *)loadData
{
    [self throwError];
    return nil;
}

//abstract
-(void)configureCell:(UITableViewCell *)cell withManagedObject:(NSManagedObject *)object
{
    [self throwError];
}

// abstract
-(NSString *)segueIdentifier
{
    [self throwError];
    return nil;
}

// abstract
-(NSString *)cellIdentifier
{
    [self throwError];
    return nil;
}

// makes string for next replacement $SEARCH to searchText and filtering *data to *filteredData
-(NSString *)predicateString
{
    [self throwError];
    return nil;
}

@end
